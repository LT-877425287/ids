package com.sencorsta.ids.core.application.zone;

import com.sencorsta.ids.core.application.Application;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.function.FunctionSystem;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.server.IdsThreadFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ZoneService extends NodeService {

    private static final int ZONE_INTERVAL = SysConfig.getInstance().getInt("zone.interval", 1000);
    private static final ExecutorService POOR = Executors.newFixedThreadPool(1,new IdsThreadFactory("update"));
    //private static final ExecutorService POOR=Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2+1);
    //private static final ExecutorService POOR=Executors.newCachedThreadPool();

    static {
        new Thread(() -> {
            while (true) {
                FunctionSystem.waitMills(ZONE_INTERVAL);
                Application.APP_TIME = System.currentTimeMillis();
                POOR.execute(() -> {
                    update();
                });
            }
        }, "ZONE").start();
    }

    private static void update() {
        for (Zone zone : Zones.values()) {
            try {
                zone.execute();
            } catch (Exception e) {
                Out.error(e);
            }
        }
    }

}
