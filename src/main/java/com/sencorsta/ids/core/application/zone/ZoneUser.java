package com.sencorsta.ids.core.application.zone;

import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;

public abstract class ZoneUser {
    public Zone zone;
    public String userId;

    public abstract void subscribe(String subscribeId);

    public abstract void unSubscribe(String subscribeId);

    public abstract void sendMsg(RpcMessage msg);
}
