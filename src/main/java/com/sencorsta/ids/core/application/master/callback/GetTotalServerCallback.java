package com.sencorsta.ids.core.application.master.callback;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessageHandler;
import io.netty.channel.Channel;

@ClientEvent("GetTotalServer.C")
public class GetTotalServerCallback extends RpcMessageHandler {

	@Override
	public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
		ProxyClient.maintenanceList(json.getJSONObject("data").getJSONObject("totalServers"));
		return empty();
	}

}
