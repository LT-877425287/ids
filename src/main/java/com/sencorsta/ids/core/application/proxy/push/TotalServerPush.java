package com.sencorsta.ids.core.application.proxy.push;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessageHandler;
import io.netty.channel.Channel;

@ClientEvent("TotalServer.push")
public class TotalServerPush extends RpcMessageHandler {

	@Override
	public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {

		ProxyClient.maintenanceList(json);

		// Out.info("服务器总列表:",json);

		return  empty();
	}

}
