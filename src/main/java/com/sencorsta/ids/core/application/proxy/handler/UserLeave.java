package com.sencorsta.ids.core.application.proxy.handler;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.application.Application;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenServerBootstrap;
import com.sencorsta.ids.core.tcp.opensocket.OpenUser;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessageHandler;
import io.netty.channel.Channel;

@ClientEvent("UserLeave")
public class UserLeave extends RpcMessageHandler {
	@Override
	public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
		String userId=json.getString("userId");
		Out.debug("开始踢用户"," userId:",userId);
		Application.getInstance().onUserLeave(userId);
		return  empty();
	}

}
