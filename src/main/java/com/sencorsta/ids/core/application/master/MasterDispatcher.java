package com.sencorsta.ids.core.application.master;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.NetHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.server.RpcMessageDispatcher;

/**
 * 逻辑分发
 * 
 * @author ICe
 */
public class MasterDispatcher extends RpcMessageDispatcher {

	public MasterDispatcher(String functionName) {
		super();
		this.functionName=functionName+"消息分发器";
	}
	
	/**
	 * 逻辑句柄处理
	 */
	public void execute(RpcMessage message) {
		Out.trace("MasterDispatcher"," execute消息: ",message,"  ",Thread.currentThread().getName());
		String method = message.method;

		NetHandler handler = method != null ? handlers.get(method) : null;
		if (handler != null) {
			Out.trace("开始处理:",method, " -> ", message);
			if (message.isChannelClosed()) {
				Out.trace("Channel已经关闭!放弃执行");
				return;
			}
			try {
				NetHandler newHandler = (NetHandler) handler.clone();
				newHandler.bindChannel(message.channel);
				newHandler.bindUserId(message.userId);
				newHandler.execute(message);
			} catch (Exception e) {
				Out.error(e.getMessage());
				e.printStackTrace();
			}
		} else {
//			IPBlacks.getInstance().exception(pak.getSession());
			Out.warn("未找到method处理类 : ", method == null ? "null" : method);
		}

	}
}
