package com.sencorsta.ids.core.application.proxy;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.Header;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * 请求消息解码类
 * @author ICe
 */
public final class ProxyDecoder extends ByteToMessageDecoder {

	private static int __RESPONSE_MAX_LEN = Integer.MAX_VALUE;
	
	public ProxyDecoder() {
		
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> packets) throws Exception {
		if(in.readableBytes() < Header.SIZE) {
			return;
		}
		in.markReaderIndex();
		
		in.readShort();
		int len=in.readInt();
		if (len > __RESPONSE_MAX_LEN || len < 0) {
			Channel session = ctx.channel();
			Out.warn("包体长度错误");
			session.close();
			return;
		}
		if (in.readableBytes() < len) {
			in.resetReaderIndex();
			return;
		}
//		byte[] temp=new byte[in.readableBytes()];
//		in.readBytes(temp);
//		Out.debug("收到数据:", Arrays.toString(temp),"总长度",temp.length);
//		in.resetReaderIndex();


		in.resetReaderIndex();
		RpcMessage msg=new RpcMessage();
		msg.decode(in);
		msg.channel=ctx.channel();
		packets.add(msg);
	}

}
