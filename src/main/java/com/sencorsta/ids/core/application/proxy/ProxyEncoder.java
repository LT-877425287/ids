package com.sencorsta.ids.core.application.proxy;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.util.Arrays;

/**
 * 发送消息编码类
 * @author ICe
 */
public final class ProxyEncoder extends MessageToByteEncoder<RpcMessage> {

	@Override
	protected void encode(ChannelHandlerContext ctx, RpcMessage msg, ByteBuf out) throws Exception {
		//out.writeBytes(msg.getContent().slice());

		msg.encode(out);
		out.resetReaderIndex();
		byte[] temp=new byte[out.readableBytes()];
		out.readBytes(temp);
		Out.trace("Rpc发送数据:", Arrays.toString(temp),"总长度",temp.length);
		out.resetReaderIndex();
		ctx.flush();

	}

}
