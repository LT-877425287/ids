package com.sencorsta.ids.core.application.proxy.handler;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenServerBootstrap;
import com.sencorsta.ids.core.tcp.opensocket.OpenUser;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessageHandler;
import io.netty.channel.Channel;

@ClientEvent("Subscribe")
public class Subscribe extends RpcMessageHandler {
	@Override
	public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
		String subscribeId=json.getString("subscribeId");
		String userId=json.getString("userId");
		String type=json.getString("type");
		Out.debug("开始订阅","subscribeId:",subscribeId," userId:",userId);
		OpenUser user=OpenServerBootstrap.getInstance().users.get(userId);
		if (user!=null){
			user.subscribe(subscribeId,type);
		}else {
			Out.debug("开始订阅失败","用户不存在:",subscribeId," userId:",userId);
		}
		return  empty();
	}

}
