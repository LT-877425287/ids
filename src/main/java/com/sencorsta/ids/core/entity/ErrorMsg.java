package com.sencorsta.ids.core.entity;

public class ErrorMsg extends BaseMsg{
	public String Msg;
	public ErrorMsg(int code, String msg) {
		super(code);
		Msg = msg;
	}

	public ErrorMsg() {
		super();
		Msg = "Success";
	}
}
