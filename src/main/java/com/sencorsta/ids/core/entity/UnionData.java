package com.sencorsta.ids.core.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface UnionData {
	public enum Type{ ALL,RedisOnly,MySqlOnly,None};
	//String name();
	Type type() default Type.None;

}
