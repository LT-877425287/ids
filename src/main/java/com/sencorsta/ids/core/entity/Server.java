package com.sencorsta.ids.core.entity;

import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.Channel;

import com.sencorsta.ids.core.configure.GlobalConfigure;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;

public class Server {

	public String type;

	public String sid;

	public String openHost;
	public String openPublicHost;
	public int openPort;

	public String backHost;
	public String backPublicHost;
	public int backPort;

	public int freeMemory;
	public int maxMemory;

	public int load;
	
	public boolean clearflag;

	private Channel channel;

	public Server() {
		
	}
	
	
	public void bind(Channel channel) {
		this.channel = channel;
		channel.attr(GlobalConfigure.__KEY_SERVER).set(this);
	}

	public Channel channel() {
		return this.channel;
	}

	public void push(RpcMessage message) {
		Out.trace("开始推送:",type," ["+backHost + ":", backPort+"]");
		this.channel.writeAndFlush(message);
	}
	

	@Override
	public String toString() {
		StringBuffer buff=new StringBuffer();
		buff.append(sid + " --> ");
		
		if (channel==null) {
			buff.append(" #未连接#");
		}else {
			buff.append(" *已连接*");
		}
		
		buff.append(" 内部地址:"+backHost +"("+backPublicHost+")"+ ":" + backPort);
		if (!StringUtil.isEmpty(openHost)) {
			buff.append(" 外部地址:"+openHost +"("+openPublicHost+")"+":" + openPort);
		}
		buff.append(" 内存占用:"+(maxMemory-freeMemory)+"MB"+"/"+maxMemory+"MB");
		return buff.toString();
	}

}
