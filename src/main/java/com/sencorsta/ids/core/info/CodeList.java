package com.sencorsta.ids.core.info;

import com.sencorsta.ids.core.entity.ErrorMsg;

/**
　　* @description: 异常码处理
　　* @author TAO
　　* @date 2019/6/12 17:43
　　*/
public class CodeList{
	//
	public static final ErrorMsg SUCCESS=new ErrorMsg(0,"成功");

	public static final ErrorMsg SERVER_ERROR=new ErrorMsg(100,"服务器异常");
}
