package com.sencorsta.ids.core.data.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 读取行回调
 * @author ICe
 */
public abstract class DBRowMapper<T> {

	public void setParams(PreparedStatement pstmt)  { }
	
	public abstract T mapRow(ResultSet rs) throws SQLException;
	
}
