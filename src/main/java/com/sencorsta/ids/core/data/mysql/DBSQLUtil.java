package com.sencorsta.ids.core.data.mysql;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.utils.string.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class DBSQLUtil {

    public static final short EQUAL = 0;
    public static final short GREATER = 1;
    public static final short LESS = 2;
    public static final short GREATER_EQUAL = 3;
    public static final short LESS_EQUAL = 4;


    public DBSQLUtil() {
        innerlist = new ArrayList<>();
    }

    public DBSQLUtil(JSONObject json) {
        innerlist = new ArrayList<>();
        json.keySet().forEach(key -> {
            //判断是否是大于或者小于
            if (key.length()>2){
                short symbolValue=-1;
                String symbol=key.substring(key.length()-2,key.length());
                String afterKey=key.substring(0,key.length()-2);
                switch (symbol){
                    case "==":
                        symbolValue=0;
                        break;
                    case ">=":
                        symbolValue=3;
                        break;
                    case "<=":
                        symbolValue=4;
                        break;
                    case ">>":
                        symbolValue=1;
                        break;
                    case "<<":
                        symbolValue=2;
                        break;
                        default:
                            break;
                }
                if (symbolValue>=0){
                    putParam(afterKey, json.get(key),symbolValue);
                }else {
                    putParam(key, json.get(key));
                }
            }else {
                putParam(key, json.get(key));
            }
        });
    }

    List<SQLObject> innerlist;

    public void putParam(String key, Object value) {
        putParam(key, value, (short) 0);
    }

    public void putParam(String key, Object value, short symbol) {
        if (value != null) {
            if (value instanceof String) {
                if (!StringUtil.isEmpty(value.toString())) {
                    innerlist.add(new SQLObject(key, value, symbol));
                }
            } else {
                innerlist.add(new SQLObject(key, value, symbol));
            }
        }
    }

    public SQLandValues makeCondition(String sql, Object[] parms) {
        List<Object> list = new ArrayList<>();
        for (int m = 0; m < parms.length; m++) {
            list.add(parms[m]);
        }

        StringBuffer sqlBuf = new StringBuffer();
        sqlBuf.append(" where 1=1 ");
        for (int i = 0; i < innerlist.size(); i++) {
            SQLObject sqlObject = innerlist.get(i);
            String symbolStr = getSymbol(sqlObject.symbol);
            sqlBuf.append(" AND " + " " + sqlObject.key + " " + symbolStr + " ? ");
            list.add(sqlObject.value);
        }

        sql += sqlBuf.toString();
        parms = list.toArray();

        return new SQLandValues(sql, parms);
    }

    public SQLandValues makeInsert(String sql, Object[] parms) {
        List<Object> list = new ArrayList<>();
        for (int m = 0; m < parms.length; m++) {
            list.add(parms[m]);
        }

        StringBuffer sqlKeyBuf = new StringBuffer();
        sqlKeyBuf.append(" ( ");

        StringBuffer sqlValueBuf = new StringBuffer();
        sqlValueBuf.append(" ( ");

        for (int i = 0; i < innerlist.size(); i++) {
            SQLObject sqlObject = innerlist.get(i);

            sqlKeyBuf.append(sqlObject.key);
            sqlValueBuf.append("?");
            list.add(sqlObject.value);

            if (i + 1 < innerlist.size()) {
                sqlKeyBuf.append(",");
                sqlValueBuf.append(",");
            }
        }

        sqlKeyBuf.append(" ) ");
        sqlValueBuf.append(" ) ");


        sql += (sqlKeyBuf.toString() + " VALUES " + sqlValueBuf.toString());
        parms = list.toArray();

        return new SQLandValues(sql, parms);
    }

    public SQLandValues makeUpdata(String sql, Object[] parms) {
        List<Object> list = new ArrayList<>();
        for (int m = 0; m < parms.length; m++) {
            list.add(parms[m]);
        }

        StringBuffer sqlBuf = new StringBuffer();


        for (int i = 0; i < innerlist.size(); i++) {
            SQLObject sqlObject = innerlist.get(i);

            sqlBuf.append(sqlObject.key+"="+"? ");
            list.add(sqlObject.value);

            if (i + 1 < innerlist.size()) {
                sqlBuf.append(",");
            }
        }

        sql += (sqlBuf.toString());
        parms = list.toArray();

        return new SQLandValues(sql, parms);
    }

    private String getSymbol(short type) {
        switch (type) {
            case EQUAL:
                return "=";
            case GREATER:
                return ">";
            case LESS:
                return "<";
            case GREATER_EQUAL:
                return ">=";
            case LESS_EQUAL:
                return "<=";
        }
        return "=";
    }


    private class SQLObject {
        String key;
        Object value;
        short symbol;

        public SQLObject(String key, Object value, short symbol) {
            this.key = key;
            this.value = value;
            this.symbol = symbol;
        }
    }

}
