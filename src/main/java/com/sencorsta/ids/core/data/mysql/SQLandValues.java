package com.sencorsta.ids.core.data.mysql;

public class SQLandValues {
    String SQL;
    Object[] values;

    public SQLandValues(String SQL, Object[] values) {
        this.SQL = SQL;
        this.values = values;
    }

    public String getSQL() {
        return SQL;
    }

    public void setSQL(String SQL) {
        this.SQL = SQL;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }
}
