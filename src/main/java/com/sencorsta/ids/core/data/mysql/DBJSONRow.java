package com.sencorsta.ids.core.data.mysql;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.object.ClassUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DBJSONRow extends DBRowMapper {

    @Override
    public JSONObject mapRow(ResultSet rs) throws SQLException {
        int count = rs.getMetaData().getColumnCount();
        JSONObject json = new JSONObject();
        for (int i = 0; i < count; i++) {
            String name = rs.getMetaData().getColumnLabel(i + 1);
            json.put(name, rs.getObject(i + 1));
        }
        return json;
    }
}
