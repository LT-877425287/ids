package com.sencorsta.ids.core.data.mysql;




import java.sql.ResultSet;
import java.sql.SQLException;

public class DBObjectRow extends DBRowMapper {

    @Override
    public Object mapRow(ResultSet rs) throws SQLException {
        int count = rs.getMetaData().getColumnCount();
        Object[] os = new Object[count];
        for (int i = 0; i < count; i++) {
            os[i] = rs.getObject(i + 1);
        }
        return os;
    }
}
