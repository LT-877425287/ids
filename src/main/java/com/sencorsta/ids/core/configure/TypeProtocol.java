package com.sencorsta.ids.core.configure;

public class TypeProtocol {
	public static final short TYPE_RPC_REQ=1;
	public static final short TYPE_RPC_RES=2;

	public static final short TYPE_REQ=3;
	public static final short TYPE_RES=4;

	public static final short TYPE_HEAT=5;

	public static final short TYPE_PUSH=6;
	public static final short TYPE_RPC_PUSH=7;
}
