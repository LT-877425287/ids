package com.sencorsta.ids.core.configure;

/**
　　* @description: 序列化类型
　　* @author TAO
　　* @date 2019/6/12 17:27
　　*/
public class TypeSerialize {
	public static final short TYPE_JSON=1;
	public static final short TYPE_PROTOBUF=2;
	public static final short TYPE_BYTEARR=3;
	public static final short TYPE_STRING=4;
}
