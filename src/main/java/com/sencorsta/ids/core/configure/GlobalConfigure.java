package com.sencorsta.ids.core.configure;

import java.io.File;
import java.nio.charset.Charset;

import com.sencorsta.ids.core.entity.Server;

import com.sencorsta.ids.core.tcp.opensocket.client.SimulationUser;
import io.netty.util.AttributeKey;

/**
 * 　　* @description: 全局配置
 * 　　* @author TAO
 * 　　* @date 2019/6/12 17:27
 *
 */
public interface GlobalConfigure {
    /**
     * 默认下行字节缓存容量
     */
    int __BODY_CAPACITY = 512;

    /**
     * 连接超时
     */
    AttributeKey<Boolean> __KEY_SESSION_TIMEOUT = AttributeKey.valueOf("__KEY_SESSION_TIMEOUT");

    /**
     * 目录结构
     */
    String _ROOT_DIR_ = System.getProperty("user.dir") + File.separator + "%s" + File.separator;

    /**
     * 游戏世界服务私有配置文件目录
     */
    String DIR_CONF_SERVER = String.format(_ROOT_DIR_, "conf");

    /**
     * 文件日志存储目录
     */
    String DIR_LOG = SysConfig.getInstance().get("log.dir", System.getProperty("user.dir") + File.separator + "logs");

    /**
     * 服务器
     */
    AttributeKey<Server> __KEY_SERVER = AttributeKey.valueOf("__KEY_SERVER");

    /**
     * 模拟用户
     */
    AttributeKey<SimulationUser> __KEY_SIMULATIONUSER = AttributeKey.valueOf("__KEY_SIMULATIONUSER");

    /**
     * 秒
     */
    int TIME_SECOND = 1000;

    /**
     * 分
     */
    int TIME_MINUTE_SECOND = 60;
    int TIME_MINUTE = TIME_MINUTE_SECOND * TIME_SECOND;

    /**
     * 时
     */
    int TIME_HOUR_SECOND = TIME_MINUTE_SECOND * 60;
    int TIME_HOUR = TIME_MINUTE * 60;

    /**
     * 天
     */
    int TIME_DAY_SECOND = TIME_HOUR_SECOND * 24;
    int TIME_DAY = TIME_HOUR * 24;

    /**
     * 周
     */
    int TIME_WEEK_SECOND = TIME_DAY_SECOND * 7;
    int TIME_WEEK = TIME_DAY * 7;

    /**
     * 月
     */
    int TIME_MONTH_SECOND = TIME_DAY_SECOND * 30;
    int TIME_MONTH = TIME_DAY * 30;

    /**
     * 年
     */
    int TIME_YEAR_SECOND = TIME_DAY_SECOND * 365;
    long TIME_YEAR = TIME_DAY * 365L;

    Charset UTF_8 = Charset.forName("UTF-8");
}
