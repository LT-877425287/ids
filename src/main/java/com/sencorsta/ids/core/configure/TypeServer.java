package com.sencorsta.ids.core.configure;

/**
　　* @description: 服务器类型
　　* @author TAO
　　* @date 2019/6/12 17:27
　　*/
public class TypeServer {
	public static final String TYPE_MASTER="MASTER";
	public static final String TYPE_GATEWAY="GATEWAY";
	public static final String TYPE_PROXY="PROXY";
	public static final String TYPE_DATA="DATA";
	public static final String TYPE_APP="APP";
}
