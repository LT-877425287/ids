package com.sencorsta.ids.core.configure;

import com.sencorsta.ids.core.function.FunctionSystem;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.file.FileUtil;
import com.sencorsta.utils.file.PropertiesUtil;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 游戏配置类
 * 
 * @author ICe
 */
public final class SysConfig {

	private static final String __0x__ = "0x";

	/** 属性列表 */
	private Map<String, String> configs;

	/** 单例 */
	private static SysConfig instance = new SysConfig();

	/** 是否已经初始化 */
	private boolean inited;

	/**
	 * 私有构造
	 */
	private SysConfig() {
		configs = new HashMap<String, String>();
	}

	/**
	 * 获取单例
	 */
	public static SysConfig getInstance() {
		if (instance.inited==false){
			instance.init();
		}
		return instance;
	}

	private boolean isEnableConnector, isEnableDB, isEnableBI, isEnableMaster, isEnableDebug;

	/**
	 * 初始化
	 */
	public synchronized void init() {
		if (!inited) {
			inited = true;
			LinkedList<String> exts = new LinkedList<String>();
			File confServer = new File("conf/server.conf");
			build(exts, confServer);
			while (!exts.isEmpty()) {
				String properties = exts.poll();
				confServer = new File(properties);

				if (confServer != null && confServer.exists() && confServer.isFile()) {
					if (properties.endsWith("properties")) {
						load(confServer);
					} else {
						build(exts, confServer);
					}
					Out.info("加载扩展配置成功：", confServer.getAbsolutePath());
				} else {
					Out.info("扩展配置不存在!  -> " + confServer.getPath());
				}
			}
			isEnableConnector = getBoolean("server.connector.enable");
			isEnableDB = getBoolean("server.db.enable");
			isEnableBI = getBoolean("server.bi.enable");
			isEnableMaster = getBoolean("server.master.enable");
			isEnableDebug = getBoolean("server.debug");
		}
		FunctionSystem.open();
		Out.trace("💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨");
		Out.info("已加载系统参数 -> " + configs.size());
		for (var key : configs.keySet()) {
			Out.trace(key, " :", configs.get(key));
		}
		Out.trace("💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨💨");
	}

	private void build(LinkedList<String> exts, File confServer) {
		List<String> confs = FileUtil.readLines(confServer);
		for (String conf : confs) {
			String cs = conf.trim();
			if (cs.length() == 0 || cs.startsWith("#")) {
				continue;
			}
			int index = conf.indexOf("=");
			if (index > 0) {
				String key = conf.substring(0, index).trim();
				String value = conf.substring(index + 1).trim();
				if (key.equals("include") && !exts.contains(value)) {
					exts.add(value);
					continue;
				}
				if (configs.containsKey(key)) {
					System.err.format("%s -> 配置重复，已使用%s中的配置!\n", key, confServer.getName());
				}
				configs.put(key, value);
			}
		}
	}

	/**
	 * 重加载所有配置
	 */
	public synchronized void reinit(boolean simple) {
		configs.clear();
		inited = false;
		init();
	}

	/**
	 * 加载配置
	 * 
	 * @param configFile 配置文件名（完整路径）
	 */
	private void load(File configFile) {
		Map<String, String> confs = PropertiesUtil.loadProperties(configFile);
		for (Map.Entry<String, String> entry : confs.entrySet()) {
			if (!configs.containsKey(entry.getKey())) {
				configs.put(entry.getKey(), entry.getValue());
			} else {
				Out.warn(entry.getKey() + " -> 配置重复,server.properties中的配置无效!");
			}
		}
	}

	/**
	 * 获取游戏服务公网IP地址
	 */
	public String getGamePubHost() {
		return exists("server.pubhost") ? get("server.pubhost") : getGameHost();
	}

	/**
	 * 获取游戏服务IP地址
	 */
	public String getGameHost() {
		return get("server.host");
	}

	/**
	 * 获得服务器侦听端口
	 */
	public int getGamePort() {
		return getInt("server.port");
	}

	/**
	 * 是否使用GM服务器
	 */
	public boolean isEnableGm() {
		return getBoolean("server.gm.enable");
	}

	/**
	 * 是否使用Proxy服务器
	 */
	public boolean isEnableConnector() {
		return isEnableConnector;
	}

	/**
	 * 是否使用DB服务器
	 */
	public boolean isEnableDB() {
		return isEnableDB;
	}

	/**
	 * 是否使用BI服务器
	 */
	public boolean isEnableBI() {
		return isEnableBI;
	}

	/**
	 * 是否使用充值服务器
	 */
	public boolean isEnableMaster() {
		return isEnableMaster;
	}

	/**
	 * 是否启用debug环境
	 */
	public boolean isEnableDebug() {
		return isEnableDebug;
	}

	/**
	 * 是否使用登录服务器
	 */
	public boolean isEnableLogin() {
		return getBoolean("server.login.enable");
	}

	/**
	 * 获取DB服务IP地址
	 */
	public String getDBHost() {
		return get("server.db.host");
	}

	/**
	 * 获得DB服侦听端口
	 */
	public int getDBPort() {
		return getInt("server.db.port");
	}

	/**
	 * 服务器最大在线人数
	 */
	public int getPlayerLimit() {
		return getInt("server.player.limit", 2000);
	}

	/**
	 * 自动保存玩家数据的延迟时间
	 */
	public int getAutoSaveDelay() {
		return getInt("auto.save.delay", 555);
	}

	/**
	 * 自动保存玩家数据的时间间隔
	 */
	public int getAutoSaveInterval() {
		return getInt("auto.save.interval", 0);
	}

	/**
	 * 获取应用ID
	 */
	public int getAppID() {
		return getInt("server.appid", 0);
	}

	/**
	 * 获取服务ID
	 */
	public int getServerID() {
		return getInt("server.id", 0);
	}

	/**
	 * 获取服务语言
	 */
	public String getLangType() {
		return get("server.lang");
	}

	/**
	 * 设置系统参数
	 */
	public void put(String key, String value) {
		configs.put(key, value);
	}

	/**
	 * 返回某个系统参数值
	 */
	public String get(String key) {
		return configs.get(key);
	}

	/**
	 * 是否存在系统参数值
	 */
	public boolean exists(String key) {
		return configs.containsKey(key);
	}

	/**
	 * 返回某个系统参数值
	 */
	public String get(String key, String def) {
		return configs.containsKey(key) ? configs.get(key) : def;
	}

	public long getLong(String key) {
		String value = configs.get(key);
		if (value.startsWith(__0x__)) {
			return Long.parseLong(value.replace(__0x__, ""), 16);
		}
		return Long.parseLong(value);
	}

	public int getInt(String key) {
		String value = configs.get(key);
		if (value.startsWith(__0x__)) {
			return Integer.parseInt(value.replace(__0x__, ""), 16);
		}
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			if (value.length() == 1) {
				char ch = value.charAt(0);
				return ch;
			} else {
				return 0;
			}
		}
	}

	public int getInt(String key, int def) {
		return configs.containsKey(key) ? getInt(key) : def;
	}

	public byte getByte(String key) {
		return Byte.parseByte(configs.get(key));
	}

	public short getShort(String key) {
		String value = configs.get(key);
		if (value.startsWith(__0x__)) {
			return Short.parseShort(value.replace(__0x__, ""), 16);
		}
		return Short.parseShort(value);
	}

	public short getShort(String key, short def) {
		return configs.containsKey(key) ? getShort(key) : def;
	}

	public boolean getBoolean(String key) {
		return Boolean.parseBoolean(configs.get(key));
	}

	public boolean getBoolean(String key, boolean def) {
		return configs.containsKey(key) ? getBoolean(key) : def;
	}

}
