package com.sencorsta.ids.core.log;

/**
 * 
 * @author ICe
 */
public enum Level {
	TRACE, DEBUG, INFO, WARN, ERROR
};
