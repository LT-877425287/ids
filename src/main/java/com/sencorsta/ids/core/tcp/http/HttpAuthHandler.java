package com.sencorsta.ids.core.tcp.http;

import com.alibaba.fastjson.JSONObject;

/**
　　* @description: Http授权处理程序
　　* @author TAO
　　* @date 2019/6/12 17:11
　　*/
public class HttpAuthHandler extends HttpHandler{

	protected  String url;
	public  void seturl(String url){
		this.url=url;
	}

	protected  JSONObject params;
	public  void setParams(JSONObject params){
		this.params=params;
	}

	public  Boolean getvalidate(){
		return true;
	}
}

