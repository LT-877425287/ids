package com.sencorsta.ids.core.tcp.http;

import com.sencorsta.ids.core.log.Out;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequestDecoder;

import java.util.List;

public class TsetHttpRequestDecoder extends HttpRequestDecoder {
    protected void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> out) throws Exception {

        buffer.markReaderIndex();

        byte[] temp=new byte[buffer.readableBytes()];
        buffer.readBytes(temp);
        Out.trace("收到HTTP数据:","总长度",temp.length);
        Out.trace("\n"+new String(temp));
        buffer.resetReaderIndex();

        super.decode(ctx,buffer,out);
    }

}
