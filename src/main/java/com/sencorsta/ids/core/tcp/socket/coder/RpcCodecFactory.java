package com.sencorsta.ids.core.tcp.socket.coder;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;


/**
 * 编解码工厂
 * @author ICe
 */
public final class RpcCodecFactory extends ChannelInitializer<SocketChannel> {

	private ChannelHandler handler;

	public RpcCodecFactory(ChannelHandler handler) {
		this.handler = handler;
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
//		pipeline.addLast("idleHandler", new IdleStateHandler(0, 0, 60));
		pipeline.addLast("decoder", new RpcDecoder());
		pipeline.addLast("encoder", new RpcEncoder());
		pipeline.addLast("handler", handler);
	}

}
