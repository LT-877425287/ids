package com.sencorsta.ids.core.tcp.opensocket;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.util.Arrays;

/**
 * 发送消息编码类
 * @author ICe
 */
public final class OpenEncoder extends MessageToByteEncoder<OpenMessage> {

	@Override
	protected void encode(ChannelHandlerContext ctx, OpenMessage msg, ByteBuf out) throws Exception {
		//out.writeBytes(msg.getContent().slice());

		msg.encode(out);
		out.markReaderIndex();
		byte[] temp=new byte[out.readableBytes()];
		out.readBytes(temp);
		Out.trace("OpenServer发送数据:", Arrays.toString(temp),"总长度",temp.length);
		out.resetReaderIndex();

		ctx.flush();
	}

}
