//package com.sencorsta.ids.core.tcp.websocket;
//
//import com.sencorsta.ids.core.tcp.opensocket.*;
//import com.sencorsta.ids.core.tcp.socket.server.RpcServerBootstrap;
//import io.netty.channel.ChannelInitializer;
//import io.netty.channel.ChannelPipeline;
//import io.netty.channel.socket.SocketChannel;
//import io.netty.handler.codec.http.HttpObjectAggregator;
//import io.netty.handler.codec.http.HttpServerCodec;
//import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
//import io.netty.handler.stream.ChunkedWriteHandler;
//
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
// * 对外服务端网络服务引导
// *
// * @author ICe
// */
//public class WsServerBootstrap extends RpcServerBootstrap {
//
//    public final Map<String, OpenUser> users = new ConcurrentHashMap();
//
//    private static WsServerBootstrap instance;
//
//    private OpenSender openSender;
//
//    public static WsServerBootstrap getInstance() {
//        if (instance == null) {
//            instance = new WsServerBootstrap();
//        }
//        return instance;
//    }
//
//    public WsServerBootstrap() {
//        super("wsServer", new ChannelInitializer<SocketChannel>() {
//            @Override
//            protected void initChannel(SocketChannel ch) throws Exception {
//                ChannelPipeline pipeline = ch.pipeline();
//
//                pipeline.addLast(new HttpServerCodec());
//                pipeline.addLast(new ChunkedWriteHandler());
//                pipeline.addLast(new HttpObjectAggregator(65535));
//                pipeline.addLast(new WebSocketServerProtocolHandler("/"));
//                pipeline.addLast(new WsServerChannelHandler());
//            }
//        });
//        super.hostStr = "wsServer.host";
//        super.minPortStr = "wsServer.port.min";
//        super.maxPortStr = "wsServer.port.max";
//        super.portStr = "wsServer.port";
//        super.hostPublicStr="wsServer.host.public";
//        openSender=new OpenSender();
//        new Thread(openSender).start();
//
//    }
//
//
//    public void addMessage(OpenMessage msg){
//        openSender.addSend(msg);
//    }
//
//
//}
