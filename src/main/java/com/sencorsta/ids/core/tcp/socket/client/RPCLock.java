package com.sencorsta.ids.core.tcp.socket.client;

import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class RPCLock {
    public Lock lock;
    public Condition condition;
    public RpcMessage message;
    public String errMsg;

    public RPCLock(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }
}
