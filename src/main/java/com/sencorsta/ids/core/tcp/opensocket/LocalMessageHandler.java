package com.sencorsta.ids.core.tcp.opensocket;

import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.configure.TypeSerialize;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessageHandler;


/**
　　* @description: 打开消息处理程序
　　* @author TAO
　　* @date 2019/6/12 17:15
　　*/
public abstract class LocalMessageHandler extends OpenMessageHandler {


	@Override
	public RpcMessage response(byte[] data, short typeSerialize) {
		RpcMessage response=new RpcMessage();
		response.header.type= TypeProtocol.TYPE_RES;
		response.channel=channel();
		response.method=method+".C";
		response.serializeType=typeSerialize;
		response.data=data;
		response.userId=userId;
		return response;
	}




}
