package com.sencorsta.ids.core.tcp.opensocket;

import io.netty.channel.Channel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class OpenUser {
    public String userId;
    public Channel channel;
    public long callCount;
    public Date LastCallTime;
    public boolean cleanIfOffline=true;
    public Set<String> Subscribes=new HashSet<>();
    public Set<String> serverTypes=new HashSet<>();

    public void sendMsg(OpenMessage msg){
        channel.writeAndFlush(msg);
    }

    public void subscribe(String subscribeId,String type){
        Subscribes.add(type+"."+subscribeId);
        serverTypes.add(type);
    }

    public void unSubscribe(String subscribeId,String type){
        Subscribes.remove(type+"."+subscribeId);
        serverTypes.remove(type);
    }


}
