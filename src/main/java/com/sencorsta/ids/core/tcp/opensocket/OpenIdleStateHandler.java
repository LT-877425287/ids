package com.sencorsta.ids.core.tcp.opensocket;

import com.sencorsta.ids.core.configure.GlobalConfigure;
import com.sencorsta.ids.core.log.Out;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;


/**
　　* @description: 打开空闲状态处理程序
　　* @author TAO
　　* @date 2019/6/12 17:14
　　*/
public class OpenIdleStateHandler extends IdleStateHandler {
    public OpenIdleStateHandler() {
        super(60*10, 0, 0);
    }

    @Override
    protected void channelIdle(ChannelHandlerContext ctx, IdleStateEvent evt) {
        Out.info(ctx.channel().remoteAddress(), " idle close!!!");
        ctx.channel().attr(GlobalConfigure.__KEY_SESSION_TIMEOUT).set(true);
        ctx.close();
        //Out.info(ctx.channel().remoteAddress(), " idle close!!!其实没有掉线 我关掉了");
    }
}
