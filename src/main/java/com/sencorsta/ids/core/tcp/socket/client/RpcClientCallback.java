package com.sencorsta.ids.core.tcp.socket.client;

import com.sencorsta.ids.core.tcp.socket.NetEvent;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;

/**
 * 处理事件接口
 * @author ICe
 */
public interface RpcClientCallback extends NetEvent {

	/** 接收到信息包 */
	void messageReceive(RpcMessage message);
	
}
