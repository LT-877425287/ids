package com.sencorsta.ids.core.tcp.websocket;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;

import java.util.Arrays;
import java.util.List;

/**
 * 发送消息编码类
 * @author ICe
 */
public final class WebSocketEncoder extends MessageToMessageEncoder<OpenMessage> {


	@Override
	protected void encode(ChannelHandlerContext channelHandlerContext, OpenMessage msg, List<Object> list) throws Exception {
		BinaryWebSocketFrame frame=new BinaryWebSocketFrame();
		ByteBuf out=frame.content();
		msg.encode(out);
		out.markReaderIndex();
		byte[] temp=new byte[out.readableBytes()];
		out.readBytes(temp);
		Out.trace("OpenServer发送数据:", Arrays.toString(temp),"总长度",temp.length);
		out.resetReaderIndex();

		list.add(frame);
	}
}
