//package com.sencorsta.ids.core.tcp.websocket;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.sencorsta.ids.core.application.Application;
//import com.sencorsta.ids.core.configure.GlobalConfigure;
//import com.sencorsta.ids.core.configure.TypeProtocol;
//import com.sencorsta.ids.core.configure.TypeSerialize;
//import com.sencorsta.ids.core.entity.CodeList;
//import com.sencorsta.ids.core.log.Out;
//import com.sencorsta.ids.core.tcp.opensocket.OpenMessage;
//import com.sencorsta.ids.core.tcp.opensocket.OpenServerBootstrap;
//import com.sencorsta.ids.core.tcp.opensocket.OpenUser;
//import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
//import com.sencorsta.utils.date.DateUtil;
//import io.netty.channel.Channel;
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.channel.ChannelInboundHandlerAdapter;
//import io.netty.channel.SimpleChannelInboundHandler;
//import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
//
//import java.nio.charset.Charset;
//
///**
// * IO处理器，所有接收到的消息put到队列里，等待处理器分发处理
// *
// * @author ICe
// */
//
///**
// * 　　* @description: 打开通道处理程序
// * 　　* @author TAO
// * 　　* @date 2019/6/12 17:14
// *
// */
//public final class WsServerChannelHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {
//
//
//    @Override
//    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
//        //Out.debug("handlerAdded Session:", ctx.channel());
//        Out.trace("WebSocket添加:", ctx.channel());
//
//        OpenUser user=new OpenUser();
//        user.channel=ctx.channel();
//        user.channelType=1;
//        user.channel.attr(OpenServerBootstrap.getInstance().KEY_OPENUSER).set(user);
//    }
//
//    @Override
//    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        Out.info("WebSocket异常:", ctx.channel(), " ", cause.getMessage());
//        cause.printStackTrace();
//        ctx.close();
//    }
//
//    @Override
//    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
//        Channel channel = ctx.channel();
//        //Out.info("handlerRemoved : ", channel);
//        Application.getInstance().onOpenClose(channel);
//        Out.trace("WebSocket移除:", ctx.channel());
//        //Application.getInstance().onServiceClose(channel);
//
//    }
//
//
//    @Override
//    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msgIn) throws Exception {
//        Out.trace("WebSocket新消息:", msgIn.text());
//        //ctx.channel().writeAndFlush(new TextWebSocketFrame("你好！！！"));
//
//        JSONObject jsonMsg= JSON.parseObject(msgIn.text());
//        String method=jsonMsg.getString("method");
//        JSONObject data=jsonMsg.getJSONObject("data");
//
//        if (method==null||data==null){
//            OpenMessage response = new OpenMessage();
//            response.header.type = TypeProtocol.TYPE_RES;
//            response.channel = ctx.channel();
//            response.method = "";
//            response.serializeType = TypeSerialize.TYPE_JSON;
//            response.data = JSON.toJSONString(CodeList.SERVER_ERROR).getBytes(GlobalConfigure.UTF_8);
//            //ctx.channel().writeAndFlush(new TextWebSocketFrame(response));
//            OpenServerBootstrap.getInstance().addMessage(response);
//            return;
//        }
//
//        OpenMessage msg=new OpenMessage();
//        msg.channel=ctx.channel();
//        msg.method=method;
//        msg.data=data.toJSONString().getBytes(Charset.forName("UTF-8"));
//        msg.serializeType= TypeSerialize.TYPE_JSON;
//        Application.getInstance().addOpenMessage(msg);
//
//
////        OpenMessage msg = (OpenMessage) packet;
////        Out.trace(" ");
////
////        switch (msg.header.type) {
////            case TypeProtocol.TYPE_REQ: {
////                Out.trace("OpenServer新消息:", ctx.channel(), " ", msg);
////                Application.getInstance().addOpenMessage(msg);
////                break;
////            }
////            case TypeProtocol.TYPE_HEAT: {
////                Out.trace("OpenServer心跳包:", ctx.channel(), " ", msg);
////                //心跳包
////                OpenMessage heart = new OpenMessage();
////                heart.header.type = TypeProtocol.TYPE_HEAT;
////                ctx.channel().writeAndFlush(heart);
////                break;
////            }
////            default:
////                break;
////        }
//    }
//
//}
