package com.sencorsta.ids.core.tcp.opensocket;

import com.alibaba.fastjson.JSON;
import com.sencorsta.ids.core.application.Application;
import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.log.Out;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;

/**
 * IO处理器，所有接收到的消息put到队列里，等待处理器分发处理
 *
 * @author ICe
 */

/**
 * 　　* @description: 打开通道处理程序
 * 　　* @author TAO
 * 　　* @date 2019/6/12 17:14
 */
public final class OpenChannelHandler extends ChannelInboundHandlerAdapter {

//	private OpenOpenServerBootstrap OpenServer;
//
//	public OpenChannelHandler(OpenOpenServerBootstrap OpenServer) {
//		this.OpenServer = OpenServer;
//	}

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        //Out.debug("handlerAdded Session:", ctx.channel());
        Out.trace("OpenServer添加:", ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Out.info("OpenServer异常:", ctx.channel(), " ", cause.getMessage());
        //cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        //Out.info("handlerRemoved : ", channel);
        Application.getInstance().onOpenClose(channel);
        Out.trace("OpenServer移除:", ctx.channel());
        //Application.getInstance().onServiceClose(channel);

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object packet) throws Exception {

        if (packet instanceof OpenMessage){
            OpenMessage msg = (OpenMessage) packet;
            Out.trace(" " + msg.toStringPlus());
            switch (msg.header.type) {
                case TypeProtocol.TYPE_REQ: {
                    Out.trace("OpenServer新消息:", ctx.channel(), " ", msg);
                    Application.getInstance().addOpenMessage(msg);
                    break;
                }
                case TypeProtocol.TYPE_HEAT: {
                    Out.trace("OpenServer心跳包:", ctx.channel(), " ", msg);
                    //心跳包
                    OpenMessage heart = new OpenMessage();
                    heart.header.type = TypeProtocol.TYPE_HEAT;
                    ctx.channel().writeAndFlush(heart);
                    break;
                }
                default:
                    Out.trace("不支持的协议类型！", msg.header.type);
                    break;
            }
        }else {
            Out.trace(packet);
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof WebSocketServerProtocolHandler.HandshakeComplete) {
            WebSocketServerProtocolHandler.HandshakeComplete complete = (WebSocketServerProtocolHandler.HandshakeComplete) evt;
            QueryStringDecoder query = new QueryStringDecoder(complete.requestUri());

            Out.trace("握手成功"+JSON.toJSONString(query.parameters()));
        }
    }

}
