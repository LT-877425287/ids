package com.sencorsta.ids.core.tcp.opensocket;

import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.configure.TypeSerialize;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessageHandler;


/**
　　* @description: 打开消息处理程序
　　* @author TAO
　　* @date 2019/6/12 17:15
　　*/
public abstract class OpenMessageHandler extends RpcMessageHandler {


	@Override
	public RpcMessage response(byte[] data, short typeSerialize) {
		RpcMessage response=new RpcMessage();
		response.header.type= TypeProtocol.TYPE_RPC_RES;
		response.channel=channel();
		response.method=method+".C";
		response.serializeType=typeSerialize;
		response.data=data;
		response.userId=userId;
		return response;
	}

	public RpcMessage responseProto(com.google.protobuf.GeneratedMessageV3.Builder res) {
		return response(protpToByte(res), TypeSerialize.TYPE_PROTOBUF);
	}

	public RpcMessage pushProto(String userId,String method,com.google.protobuf.GeneratedMessageV3.Builder res) {
		RpcMessage response=pushBytes(userId,method,protpToByte(res), TypeSerialize.TYPE_PROTOBUF);
		response.method=method;
		response.userId=userId;
		return response;
	}


	public RpcMessage pushBytes(String userId,String method,byte[] data, short typeSerialize) {
		RpcMessage puls=new RpcMessage();
		puls.header.type= TypeProtocol.TYPE_RPC_RES;
		puls.channel=channel();
		puls.method=method;
		puls.serializeType= typeSerialize;
		puls.data=data;
		puls.userId=userId;
		return puls;
	}



}
