package com.sencorsta.ids.core.tcp.opensocket;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.configure.GlobalConfigure;
import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.configure.TypeSerialize;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.BaseMessage;
import com.sencorsta.ids.core.tcp.socket.protocol.Body;
import io.netty.channel.Channel;

import java.util.Arrays;

/**
　　* @description: 打开消息
　　* @author TAO
　　* @date 2019/6/12 17:15
　　*/
public class OpenMessage extends BaseMessage {

    // public short serverType;//服务器类型
    public short serializeType;// 序列化类型
    public String method;// 调用方法
    public byte[] data;// 数据

    public Channel channel;

    public OpenMessage() {
        data=new byte[0];
    }

    /**
     * 是否已关闭
     */
    public boolean isChannelClosed() {
        return channel == null || !channel.isActive();
    }

    @Override
    public void encodeBody() {
        if (header.type==TypeProtocol.TYPE_HEAT){
            header.length = 0;
            body = new Body(0);
        }else {
            body = new Body(10);
            body.content.writeShort(serializeType);
            body.writeString(method);
            body.content.writeBytes(data);
            header.length = body.content.readableBytes();
            //header.type = TypeProtocol.TYPE_RES;
        }
    }

    @Override
    public void decodeBody() {
        if (body.content.readableBytes()==0){
            //心跳包
            return;
        }
        serializeType = body.content.readShort();
        method = body.readString();
        data = new byte[body.content.readableBytes()];
        body.content.readBytes(data);
    }

    public String toStringPlus() {
        switch (serializeType) {
            case TypeSerialize.TYPE_JSON:
                return "header:[" + header + "]"+" body:["+"serializeType:" + serializeType+ " method:" + method+ " data:" + new String(data, GlobalConfigure.UTF_8)+"]";
            case TypeSerialize.TYPE_STRING:
                return "header:[" + header + "]"+" body:["+"serializeType:" + serializeType+ " method:" + method+ " data:" + new String(data,GlobalConfigure.UTF_8)+"]";
            case TypeSerialize.TYPE_BYTEARR:
                return "header:[" + header + "]"+" body:["+"serializeType:" + serializeType+ " method:" + method+ " data:" + Arrays.toString(data)+"]";
            case TypeSerialize.TYPE_PROTOBUF:
                return "header:[" + header + "]"+" body:["+"serializeType:" + serializeType+ " method:" + method+ " data:" + Arrays.toString(data)+"]";
            default:
                break;
        }
        return "header:[" + header + "]"+" body:["+"serializeType:" + serializeType+ " method:" + method+ " data:" + "🐷未知协议🐷"+"]";
    }

    public String toString() {
        switch (serializeType) {
            case TypeSerialize.TYPE_JSON:
                return "{[" + header + "]"+"[TYPE_JSON]"+method+"}";
            case TypeSerialize.TYPE_STRING:
                return "{[" + header + "]"+"[TYPE_STRING]+"+method+"}";
            case TypeSerialize.TYPE_BYTEARR:
                return "{[" + header + "]"+"[TYPE_BYTEARR]"+method+"}";
            case TypeSerialize.TYPE_PROTOBUF:
                return "{[" + header + "]"+"[TYPE_PROTOBUF]"+method+"}";
            default:
                break;
        }
        return "{[" + header + "]"+"[未知协议🐷]"+method+"}";
    }

    public String toJSONString(){
        JSONObject temp=new JSONObject();
        temp.put("data", JSON.parse(new String(data, GlobalConfigure.UTF_8)));
        temp.put("method",method);
        return temp.toJSONString();
    }

}
