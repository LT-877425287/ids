package com.sencorsta.ids.core.tcp.opensocket.client;

import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessage;
import com.sencorsta.ids.core.tcp.opensocket.OpenUser;

public class SimulationUser extends OpenUser {


    public void onStarted(){
        Out.trace("注意：没有重写此方法：onStarted");

    }

    public void onUpdate(){
        Out.trace("注意：没有重写此方法：onUpdate");
    }

    public void onRemoved() {
        Out.trace("注意：没有重写此方法：onRemoved");
    }

    public void onMessage(OpenMessage msg){
        Out.trace("注意：没有重写此方法：onMessage");
    }

    public void sendHeart() {
        OpenMessage heart = new OpenMessage();
        heart.header.type = TypeProtocol.TYPE_HEAT;
        sendMsg(heart);
    }

}
