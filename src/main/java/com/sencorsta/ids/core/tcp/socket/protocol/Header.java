package com.sencorsta.ids.core.tcp.socket.protocol;

/**
 * 通讯协议包头
 * @author ICe
 */
public class Header {
	
	/** 协议类型 */
	public short type;
	
	/** 协议包体字节数 */
	public int length;

	/** 包头的字节数 */
	public final static byte SIZE = 6;
	
	public Header() {
		type=0;
		length=0;
	}	
	
	public String toString() {
		return "type:"+type+" length:"+length;
	}
	
}
