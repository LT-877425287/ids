package com.sencorsta.ids.dataUnion.entity;

import com.sencorsta.ids.core.entity.UnionData;
import com.sencorsta.ids.dataUnion.util.ObjectUtil;

import java.util.Date;
import java.util.Map;
import java.util.Set;

@UnionData(type = UnionData.Type.ALL)
public class User {
    public String userId;
    public String name;
    public Integer age;
    public Date loginTime;

    public static void main(String[] args) {
        User user=new User();

        UnionData unionData=user.getClass().getAnnotation(UnionData.class);

        boolean isMySql=false;
        boolean isRedis=false;
        if (unionData.type().equals(UnionData.Type.ALL)){
            isMySql=true;
            isRedis=true;
        }else if (unionData.type().equals(UnionData.Type.MySqlOnly)){
            isMySql=true;
        }else if(unionData.type().equals(UnionData.Type.RedisOnly)){
            isRedis=true;
        }

//        if (isMySql){
//            Map<String,String> map = ObjectUtil.getProperty(user);
//            Set<String> set = map.keySet();
//            for(String key : set){
//                System.out.println(key);
//            }
//            StringBuffer sb = new StringBuffer("");
//            sb.append("CREATE TABLE `" + tableName + "` (");
//            sb.append(" `id` int(11) NOT NULL AUTO_INCREMENT,");
//            Map<String,String> map = ObjectUtil.getProperty(obj);
//            Set<String> set = map.keySet();
//            for(String key : set){
//                sb.append("`" + key + "` varchar(255) DEFAULT '',");
//            }
//            sb.append(" `tableName` varchar(255) DEFAULT '',");
//            sb.append(" PRIMARY KEY (`id`)");
//            sb.append(") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
//            try {
//                jt.update(sb.toString());
//                return 1;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return 0;
//
//            //ObjectUtil.getProperty();
//            //update mysql
//        }
        if (isRedis){
            //update redis
        }

    }
}
