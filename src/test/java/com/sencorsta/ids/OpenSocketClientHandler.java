package com.sencorsta.ids;

import com.sencorsta.ids.core.tcp.opensocket.OpenMessage;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 回调服务器的反馈信息
 * @author ICe
 */
@Sharable
public class OpenSocketClientHandler extends ChannelInboundHandlerAdapter {


	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		System.out.println("Client异常:"+ctx.channel()+cause.getMessage());
		ctx.channel().close();
	}

	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Client添加:"+ctx.channel());
	}

	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Client移除:"+ctx.channel());
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object packet) throws Exception {
		System.out.println("");
		System.out.println("Client收到消息:"+ctx.channel()+packet);
		OpenMessage msg=(OpenMessage) packet;
		System.out.println(msg.toStringPlus());
	}

}