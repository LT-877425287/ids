package com.sencorsta.ids.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueTest {
	public static BlockingQueue<String> q;
	public static void main(String[] args) {
		q=new LinkedBlockingQueue<>(65535);
		
		MqMaker maker1=new MqMaker();
		MqMaker maker2=new MqMaker();
		MqMaker maker3=new MqMaker();
		
		MqWorker worker1=new MqWorker();
		MqWorker worker2=new MqWorker();
		MqWorker worker3=new MqWorker();
		
		new Thread(maker1).start();
		new Thread(maker2).start();
		new Thread(maker3).start();
		new Thread(worker1).start();
		new Thread(worker2).start();
		new Thread(worker3).start();
		
	}
	
}
