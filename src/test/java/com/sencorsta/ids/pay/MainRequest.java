package com.sencorsta.ids.pay;


import com.sencorsta.ids.pay.HttpRequest;

import java.util.Date;
import java.util.Random;

/**
 * 请求腾飞支付Demo
 *
 * @author TFpay
 * @version 2.0
 */
public class MainRequest {
    //key 秘钥
    private static final String KEY = "0cd083b01b489b740f20cbe1e8927d61c9832674";
    // 商户ID:可在支付商户后台获取，如：100010
    private static final String MCHID = "102487";

    public static void main(String[] args) {
        String orderNumber=new Date().getTime()+""+new Random().nextInt(1000);
        System.out.println("订单号："+orderNumber);

        // 获取订单
        getMyGateWay(orderNumber);
        //查询订单
        getMyQuery( orderNumber);
        // 回调订单
        getMyCallBackUrl("http://127.0.0.1:8020/test/HelloWorld",orderNumber);
    }

    /**
     * 获取支付页面
     */
    private static void getMyGateWay(String OUT_TRADE_NO) {
        String payType = "wechat_qr", notifyUrl = "www.google.com", userCod = "tf_member";
        Double orderAmount = 1005.00D;
        // 校验长度  以及正则匹配规则
        int tradeNo = HttpRequest.verifyOutTradeNo(OUT_TRADE_NO);
        //支付类型 0为支付宝 1为微信
        payType = HttpRequest.getPayType(1);
        if (tradeNo == 200) {
            String reslut = HttpRequest.getGateWay(MCHID, OUT_TRADE_NO, payType, orderAmount, notifyUrl, userCod, KEY);
            System.out.println("获取订单 ：" + reslut);
        }
    }

    /**
     * 查询订单
     */
    private static void getMyQuery(String OUT_TRADE_NO) {
        // 校验长度  以及正则匹配规则
        int tradeNo = HttpRequest.verifyOutTradeNo(OUT_TRADE_NO);
        if (tradeNo == 200) {
            String reslut = HttpRequest.getQuery(MCHID, OUT_TRADE_NO, KEY);
            System.out.println("查询订单 ：" + reslut);
        }
    }


    /**
     * 回调地址
     *
     * @param backUrl 地址由getMyGateWay()方法中的pay_url生成
     */
    private static void getMyCallBackUrl(String backUrl,String OUT_TRADE_NO) {
        // 校验长度  以及正则匹配规则
        int tradeNo = HttpRequest.verifyOutTradeNo(OUT_TRADE_NO);
        if (tradeNo == 200) {
            Double orderAmount = 10.00D;
            Double payAmount = 10.00D;
            String reslut = HttpRequest.getCallBackUrl(backUrl, MCHID, OUT_TRADE_NO, orderAmount, payAmount, KEY);
            System.out.println("回调地址 ：" + reslut);
        }
    }

}
