package com.sencorsta.ids.pay;

import java.util.Comparator;
/**
 * 解析map 工具类
 * @author TFpay
 * @version 2.0
 */
public class MapKeyComparator implements Comparator<String> {
    @Override
    public int compare(String str1, String str2) {
        return str1.compareTo(str2);
    }
}
