package com.sencorsta.ids.pos;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.util.Arrays;

/**
 * 发送消息编码类
 *
 * @author ICe
 */
public final class PosEncoder extends MessageToByteEncoder<PosMassage> {

    @Override
    protected void encode(ChannelHandlerContext ctx, PosMassage msg, ByteBuf out) throws Exception {
        //out.writeBytes(msg.getContent().slice());
        out.writeBytes(msg.arr);
        out.markReaderIndex();
        byte[] temp = new byte[out.readableBytes()];
        out.readBytes(temp);
        Out.trace("OpenServer发送数据:", Arrays.toString(temp), "总长度", temp.length);
        Out.trace("Hex:" + My8583Ans.bytesToHexString(temp));
        Out.trace("String:" + new String(temp));

//        for (int i = 1; i < temp.length; i++) {
//            for (int j = 0; j < temp.length; ) {
//                byte[] newData;
//                newData = Arrays.copyOfRange(temp, j, j+i);
//                j += i;
//                Out.trace(new String(newData));
//            }
//        }

        out.resetReaderIndex();

        ctx.flush();
    }

}
